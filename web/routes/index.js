var express = require('express');
var _ = require('underscore');
var os = require("os");
var router = express.Router();
var handlebars = require('handlebars');
var basex = require('basex');
var pd = require('pretty-data').pd;
var teiNamespace = "declare default element namespace 'http://www.tei-c.org/ns/1.0';";
var client = new basex.Session("127.0.0.1", 1984, "admin", "admin");
client.execute("OPEN Colenso", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        console.log(data);
    }
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Colenso Project', username: "Bob"});
});

router.get('/xmlstats/', function (req, res, next) {
    res.render('xmlstats', {title: 'DB Stats'})
});


//Find function routes TODO: move these
router.get('/find/', function (req, res, next) {
    res.render('find', {title: 'Database Search', formatting: true})
});

router.get('/find/results', function (req, res, next) {
    var searchTerm = req.query.term;

    //Return to search page if nothing is searched
    if (!searchTerm || searchTerm == "" || searchTerm == " ") {
        res.redirect("/find/")
    } else {
        //Do search
        var search_string = teiNamespace;
        var iSearchTemplate = " /descendant::*:";

        var searchTermSplit = searchTerm.split(" ");
        for (var token in searchTermSplit) {
            search_string += iSearchTemplate + searchTermSplit[token] + " |";
        }

        search_string += ' /descendant-or-self::*[text() contains text "' + searchTerm + '"]';

        //client.execute(search_string, function (err, data) {
        //    if (err) {
        //        res.render('result', {term: req.query.term, error: JSON.stringify(err)});
        //    } else {
        //        var resSplit = data.result.split("\r\n");
        //        res.render('result', {term: req.query.term, data: resSplit});
        //    }
        //});
        var query = client.query(search_string);
        console.log("Executed: ", search_string);
        query.results(function (err, data) {
            if (err) {
                res.render('findresult', {title: "Find Results", term: req.query.term, error: JSON.stringify(err)});
            } else {
                if (req.query.formatting != "on") {
                    res.render('findresult', {
                        title: "Find Results",
                        term: req.query.term,
                        formatting: false,
                        fdata: data.result
                    });
                } else {
                    var erray = data.result;
                    var length = erray.length;
                    for (var i = 0; i < length; i++) {
                        if (erray[i].indexOf('<title xmlns="http://www.tei-c.org/ns/1.0">') > -1) {
                            erray[i] = erray[i].replace('<title xmlns="http://www.tei-c.org/ns/1.0">',
                                '<ftitle xmlns="http://www.tei-c.org/ns/1.0">');
                        }
                        if (erray[i].indexOf('</title>') > -1) {
                            erray[i] = erray[i].replace('</title>',
                                '</ftitle>');
                        }
                    }
                    res.render('findresult', {
                        title: "Find Results",
                        term: req.query.term,
                        data: data.result,
                        formatting: true
                    });
                }
            }
        });
    }
});

//Xquery
router.get('/xquery/', function (req, res, next) {
    res.render('xquery', {title: "Colenso Query", formatting: true});
});

router.get('/xquery/query/', function (req, res, next) {
    res.redirect('/xquery/');
});

router.post('/xquery/query/', function (req, res, next) {
    var queryRaw = req.body.code;
    var queryTei = teiNamespace + ' ' + queryRaw;

    if (!queryRaw || queryRaw == "" || queryRaw == " ") {
        res.redirect('/xquery/')
    } else {
        var query = client.query(queryTei);
        query.results(function (err, data) {
            if (err) {
                res.render('xquery', {
                    code: req.body.code,
                    error: '<h1>Error</h1><b>' + err + '</b>',
                    formatting: req.body.formatting == "on"
                })
            } else if (data.result.length == 0) {
                res.render('xquery', {
                    code: req.body.code,
                    error: "No results found, is the query valid?",
                    formatting: req.body.formatting == "on"
                });
            } else {
                if (req.body.formatting != "on") {
                    res.render('xquery', {
                        code: req.body.code,
                        formatting: false,
                        fdata: data
                    });
                } else {
                    var erray = data.result;
                    var length = erray.length;
                    for (var i = 0; i < length; i++) {
                        if (erray[i].indexOf('<title xmlns="http://www.tei-c.org/ns/1.0">') > -1) {
                            erray[i] = erray[i].replace('<title xmlns="http://www.tei-c.org/ns/1.0">',
                                '<ftitle xmlns="http://www.tei-c.org/ns/1.0">');
                        }
                        if (erray[i].indexOf('<title>') > -1) {
                            erray[i] = erray[i].replace('<title>',
                                '<ftitle>');
                        }
                        if (erray[i].indexOf('</title>') > -1) {
                            erray[i] = erray[i].replace('</title>',
                                '</ftitle>');
                        }
                    }
                    data.result = erray;
                    res.render('xquery', {title: "Query Results", code: req.body.code, data: data, formatting: true});
                }
            }
        });
    }
});

//browse
router.get('/browse/', function (req, res, next) {
    client.execute("LIST Colenso", function (err, data) {
        var files = data.result.split(os.EOL);
        files.splice(0, 2);
        files.splice(files.length - 2, files.length);
        var objArray = [];
        var doneNames = [];
        for (var i = 0; i < files.length; i++) {
            var splitObj = files[i].split(" ");
            var filePath = splitObj[0];
            //check if matches folder
            var fileFolder = filePath.split("/")[0];
            if (!_.contains(doneNames, fileFolder) && fileFolder != "" && fileFolder != "Resources.") {
                doneNames.push(fileFolder);
                var fileObj = {name: fileFolder, path: "/browse/folder/" + fileFolder};
                objArray.push(fileObj);
            }
        }
        res.render('browse-root', {title: "Colenso Browse", directory: objArray});
    })
});

router.get('/browse/file/*', function (req, res, next) {
    var filename = req.originalUrl.replace("/browse/file/", "");
    var query = client.query("for $doc in collection('Colenso') where matches(document-uri($doc), '" + filename + "') return $doc");
    query.results(function (err, data) {
        var forHtml;
        var rawish;
        var rawData = data.result[0];
        if (rawData.indexOf('<title xmlns="http://www.tei-c.org/ns/1.0">') > -1) {
            rawData = rawData.replace('<title xmlns="http://www.tei-c.org/ns/1.0">',
                '<ftitle xmlns="http://www.tei-c.org/ns/1.0">');
        }
        if (rawData.indexOf('<title>') > -1) {
            rawData = rawData.replace('<title>',
                '<ftitle>');
        }
        if (rawData.indexOf('</title>') > -1) {
            rawData = rawData.replace('</title>',
                '</ftitle>');
        }
        forHtml = rawData;
        rawish = pd.xml(data.result[0], 4);
        res.render('browse-file', {title: "Colenso Browse", file: forHtml, rawFile: rawish, filename: filename});
    });
});

router.get('/browse/folder/*', function (req, res, next) {
    var foldername = req.originalUrl.replace("/browse/folder/", "");
    var queryStr = "for $doc in collection('Colenso') where matches(document-uri($doc), '/" + foldername + "/') return db:path($doc)";
    var query = client.query(queryStr);
    query.results(function (err, data) {
        var files = data.result;
        var objArray = [];
        for (var i = 0; i < files.length; i++) {
            var filePath = files[i];
            filePath = filePath.replace(foldername + "/", "");
            var splitPath = filePath.split("/");
            var fPath = splitPath[0];
            if (!_.contains(objArray, fPath)) {
                objArray.push(fPath);
            }
        }
        var finalArray = [];
        for (var y = 0; y < objArray.length; y++) {
            var f = objArray[y];
            if (f.indexOf(".xml") > -1) {
                var fO1 = {path: f, url: "/browse/file/" + foldername + "/" + f};
                finalArray.push(fO1);
            } else {
                var fO2 = {path: f, url: "/browse/folder/" + foldername + "/" + f};
                finalArray.push(fO2);
            }
        }
        var splitFolder = foldername.split("/");
        var back = "/browse/folder/" + foldername.replace((splitFolder[splitFolder.length - 1]), "");
        back = back.substring(0, back.length - 1);
        if (back == "/browse/folder") {
            back = "/browse"
        }
        console.log("back", back);
        res.render('browse-folder', {directory: finalArray, back: back});
    });
});

//upload
router.get('/upload/', function (req, res, next) {
    res.render('upload', {});
});

router.post('/upload/submit', function (req, res, next) {
    var cmd = "";
    var code = req.body.code;
    var filePath = req.body.directory;
    client.add(filePath, code, function (err, data) {
        if (err) {
            res.render('upload-result', {
                title: 'Upload Failed',
                message: "Failed to upload to " + filePath,
                error: JSON.stringify(err, 4)
            })
        } else {
            res.render('upload-result', {
                title: 'Upload Result',
                message: "File was uploaded to " + filePath,
                data: data.result
            })
        }
    });
});

router.get('/browse/delete', function (req, res, next) {
    var files = req.query;
    var fileName = req.query.origin.replace("/browse/file", "");
    client.execute("DELETE " + fileName, function (err, data) {
        console.log("delete results", err, data);
        res.render('delete', {fileName: fileName, message: JSON.stringify(data.result, 4)});
    });
});

module.exports = router;
console.log("Live");